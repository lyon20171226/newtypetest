sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel"
], function(Controller,MessageToast,ResourceModel) {
	"use strict";

	return Controller.extend("nnext.Newtype.HelloWorld.controller.App", {
       onClickz: function(){
          var oBundle = new ResourceModel({
            bundleName: "nnext.Newtype.HelloWorld.i18n.i18n"
          });
          
       	  MessageToast.show(oBundle.getProperty("helloWorld"));
       }
	});
});